class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?
    def after_sign_in_path_for(resource)
      general_setting_index_path
    end
  
    def after_sign_out_path_for(resource)
      general_setting_index_path
    end
    
    protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, 
      keys: [:username, :email, :password, :password_confirmation])
      devise_parameter_sanitizer.permit(:account_update, 
      keys: [:username, :email, :password_confirmation, :current_password])
    end

end
