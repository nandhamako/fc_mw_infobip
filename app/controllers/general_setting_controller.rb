class GeneralSettingController < ApplicationController

    # Hack csrf Request 
    skip_before_action :verify_authenticity_token, only: [:download_image, :delete_image]  

    before_action :authenticate_user!, except: [:download_image, :delete_image]  
    #Download Image Request

    def download_image
        api_key = GeneralSetting.first.apikey
        Rails.logger.info "~"*100
        Rails.logger.info api_key
        Rails.logger.info "~"*100
        response = HTTParty.get(params[:image_url],
            headers: {
                "Authorization" => "App #{api_key}",
            "Content-Type" => "application/json"
            }) 
        Rails.logger.info response.code
        Rails.logger.info "~"*100
        Rails.logger.info response.headers
        Rails.logger.info "~"*100
        Rails.logger.info response.body
            
            if (response.code == 200 || response.code == 201 || response.code == 200)
                file_extension = response.headers["content-type"].split("/").last

                file_name = "#{Time.now.strftime("%d_%m_%Y_%I%M%S")}.#{file_extension}"
                
                File.open("public/#{file_name}", "wb") do |f|
                    f.write(response.body)
                end
                
                #puts file_name
                
                file_url = request.base_url + "/" + file_name
                # file_url = request.base_url + ActionController::Base.helpers.asset_path(file_name).to_s
                render :json => {success: true, image_url: file_url, file_name: file_name}
            else
                render :json => {:message => "please check Api_key"}
            end
    end

    def delete_image

       file =  File.delete(params[:file_name]) if File.exist?(params[:file_name])

       render :json => {success: true}

    end


    def index
        @general = GeneralSetting.first
    end

    def show
        @general = GeneralSetting.find(params[:id])
    end

    def new
        @general = GeneralSetting.new()
    end

    def create
        @general = GeneralSetting.new(general_params)

        if @general.save
            redirect_to :action => "index", :notice => "Apikey saved successfully"
        end

    end

    def edit
        @general = GeneralSetting.find(params[:id])
    end

    def update
        @general = GeneralSetting.find(params[:id])

        if @general.update(general_params)
            redirect_to :action => "index", :notice => "Apikey updated successfully"
        end

    end

    private

    def general_params
        params.require(:general_setting).permit(:apikey)
    end

end
