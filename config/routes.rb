Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources "general_setting"

  post "/api/download_image", to: "general_setting#download_image"
  post "/api/delete_image", to: "general_setting#delete_image"

  root "general_setting#index"

end
