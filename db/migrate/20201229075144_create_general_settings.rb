class CreateGeneralSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :general_settings do |t|
      t.string :apikey

      t.timestamps
    end
  end
end
